<?php

class Request {
	//Gather information
	static function ret($param, $detect = false, $nullout = false) {
		if ($detect) {
			if (isset($_REQUEST[$param])) {
				return $_REQUEST[$param];
			}
			return $nullout;
		} else {
			if (isset($_REQUEST[$param])) {
				return $_REQUEST[$param];
			}
			return $nullout;
		}
	}

	//Is index set?
	static function is_set($param) {
		if (isset($_REQUEST[$param])) {
			return true;
		}
		return false;
	}

	//We get everything back...
	static function retall() {
		return $_REQUEST;
	}
}

class Get {
	//Gather information
	static function ret($param, $detect = false, $nullout = false) {
		if ($detect) {
			if (isset($_GET[$param])) {
				return $_GET[$param];
			}
			return $nullout;
		} else {
			if (isset($_GET[$param])) {
				return $_GET[$param];
			}
			return $nullout;
		}
	}

	//Is index set?
	static function is_set($param, $choice = array()) {
		if (!empty($choice)) {
			if (isset($_GET[$param])) {
				return $choice[0];
			}
			return $choice[1];
		} else {
			if (isset($_GET[$param])) {
				return $_GET[$param];
			}
			return false;
		}
	}

	//We get everything back...
	static function retall() {
		return $_GET;
	}
}

//POST class. Same as GET with a difference that it looks into _POST
class Post {
	static function ret($param, $detect = false, $nullout = false) {
		if ($detect) {
			if (isset($_POST[$param])) {
				return $_POST[$param];
			}
			return $nullout;
		} else {
			if (isset($_POST[$param])) {
				return $_POST[$param];
			}
			return $nullout;
		}
	}

	static function is_set($param, $choice = array()) {
		if (!empty($choice)) {
			if (isset($_POST[$param])) {
				return $choice[0];
			}
			return $choice[1];
		} else {
			if (isset($_POST[$param])) {
				return true;
			}
			return false;
		}
	}

	static function retall() {
		return $_POST;
	}
}

//Cookies class.
class Cookie {
	static function ret($param, $nullout = false) {
		if (isset($_COOKIE[$param])) {
			return $_COOKIE[$param];
		}
		return $nullout;
	}

	static function is_set($param, $choice = array()) {
		if (!empty($choice)) {
			if (isset($_COOKIE[$param])) {
				return $choice[0];
			}
			return $choice[1];
		} else {
			if (isset($_COOKIE[$param])) {
				return true;
			}
			return false;
		}
	}
}
