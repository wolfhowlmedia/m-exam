<?php
//Template rendering class
class Template {
	private $out = '';
	private $_data = array();

	//Load template
	public function __construct($file, $module_data = null) {
		$this->templatefile = $file;
		$this->module_data = $module_data;
	}

	//Cleanup HTML
	static public function html($input) {
		return htmlspecialchars($input, ENT_QUOTES, 'UTF-8');
	}

	//Cleanup URL
	static public function url($input) {
		return urlencode($input);
	}

	//Add non-standard data
	public function set_data($key, $value) {
		$this->_data[$key] = $value;
	}

	private function get_data($key, $default = array()) {
		if (!isset($this->_data[$key])) {
			return $default;
		} else {
			return $this->_data[$key];
		}
	}

	//Render template or return 404
	public function render() {
		if (is_null($this->templatefile)) {
			if (isset($this->module_data['post'])) {
				echo $this->module_data['post'];
				die;
			}
			if (isset($this->module_data['get'])) {
				echo $this->module_data['get'];
				die;
			}
		} else if (file_exists(TEMPLATE_PATH . $this->templatefile)) {
			$this->_data['r_module'] = $this->module_data;
			ob_start();
			include TEMPLATE_PATH . $this->templatefile;
			$this->out = ob_get_clean();
		} else {
			echo 'File: '.TEMPLATE_PATH . $this->templatefile . ' doesn\'t exist';
		}

		return $this->out;
	}
}
