<?php
/***
 * Common class
 */
class Common {
	//Page redirect. Send header 301 MOVED
	static public function redirect($location) {
		header('location: '.$location);
		die;
	}
	
	//Is the email formed correctly?
	public static function check_email($email) {
		$pattern = 
			'/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])'.
			'(([a-z0-9-])*([a-z0-9]))+' . 
			'(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
		return (bool)preg_match($pattern, $email);
	}
	
	//The following methods are used for parsing the module name
	public static function split_pagename ($page) {
		if (preg_match("/\s/", $page))
			return $page;           // Already split --- don't split any more.

		// FIXME: this algorithm is Anglo-centric.
		static $RE;
		if (!isset($RE)) {
			// This mess splits between a lower-case letter followed by either an upper-case
			// or a numeral; except that it wont split the prefixes 'Mc', 'De', or 'Di' off
			// of their tails.
			$RE[] = '/([[:lower:]])((?<!Mc|De|Di)[[:upper:]]|\d)/';
			// This the single-letter words 'I' and 'A' from any following capitalized words.
			$RE[] = '/(?: |^)([AI])([[:upper:]][[:lower:]])/';
			// Split numerals from following letters.
			$RE[] = '/(\d)([[:alpha:]])/';

			foreach ($RE as $key => $val)
				$RE[$key] = self::pcre_fix_posix_classes($val);
		}

		foreach ($RE as $regexp) {
			$page = preg_replace($regexp, '\\1 \\2', $page);
		}

		return $page;
	}


	private static function pcre_fix_posix_classes ($regexp) {
		// First check to see if our PCRE lib supports POSIX character
		// classes.  If it does, there's nothing to do.
		if (preg_match('/[[:upper:]]/', 'A'))
			return $regexp;

		static $classes = array(
					'alnum' => "0-9A-Za-z\xc0-\xd6\xd8-\xf6\xf8-\xff",
					'alpha' => "A-Za-z\xc0-\xd6\xd8-\xf6\xf8-\xff",
					'upper' => "A-Z\xc0-\xd6\xd8-\xde",
					'lower' => "a-z\xdf-\xf6\xf8-\xff"
					);

		$keys = join('|', array_keys($classes));

		return preg_replace("/\[:($keys):]/e", '$classes["\1"]', $regexp);
	}
	
	/**
	* Generates an UUID
	* Remarks from Primoz: The original generator doesn't generate standard UUID4
	*
	* @author     Anis uddin Ahmad <admin@ajaxray.com>
	* @author     Primoz Anzur <stormchaser1@gmail.com> 
	* @return     string  the formatted uuid
	*/
	static public function uuid4() {
		$chars = sha1(uniqid(mt_rand(), true));
		$ystr = array('8', '9', 'a','b');

		$uuid  = substr($chars,0,8) . '-';
		$uuid .= substr($chars,8,4) . '-';
		$uuid .= 'a'.substr($chars,12,3) . '-';
		$uuid .= $ystr[mt_rand(0, 3)].substr($chars,16,3) . '-';
		$uuid .= substr($chars,20,12);
		return $uuid;
	}

	static public function translate($string, $lang = null) {
		global $lc_message;
		if (is_null($lang)) {
			if (!defined('LANGUAGE')) {
				return $string;
			} else {
				if (!file_exists(LANGUAGE_PATH.'/'.LANGUAGE.'.php')) {
					return $string;
				}

				include_once LANGUAGE_PATH.'/'.LANGUAGE.'.php';
				if (isset($lc_message[$string])) {
					return $lc_message[$string];
				} else {
					return $string;
				}
			}
		} else {
			if (!file_exists(LANGUAGE_PATH.'/'.$lang.'.php')) {
				return $string;
			}
			include_once LANGUAGE_PATH.'/'.$lang.'.php';
			if (isset($lc_message[$string])) {
				return $lc_message[$string];
			} else {
				return $string;
			}
		}
	}

	/**
	* A simple resolver.
	*
	* @param default_paths fill dummy paths / witches
	*
	* @return array method / path
	*/
	static public function path_router($default_paths = 2) {
		$path = array();
		for($x = 0; $x < $default_paths; $x++) {
			$path[] = '';
		}

		if (isset($_SERVER['PATH_INFO'])) {
			$path = explode('/', trim($_SERVER['PATH_INFO'], '/'));
		} else if (isset($_SERVER['REQUEST_URI'])) {
			$path = explode('/', trim(str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['REQUEST_URI']), '/'));
		} else if (isset($_SERVER['DOCUMENT_URI'])) {
			$path = explode('/', trim(str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['DOCUMENT_URI']), '/'));
		} else {
			die('Problem establishing proper routing method.');
		}
		return $path;
	}
}