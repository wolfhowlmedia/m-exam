<?php
//get DB information
require_once '../config.php';

//require command line interpreter class
require_once '../classes/class.commandline.php';

//require core converter class
require_once '../classes/class.converter.php';

//iniitialize new instance of Converter
$converter_instance = new Converter;

$switches = array(
	'-d' => array('dry_run' => true),
	'-debug' => array('debug' => true),
	'-c' => array('check_file' => false),
	'-h' => "Usage: converter.php filename <flags>
-d     Enable dry run (no data is inserted into database)
-c     Skip checking file integrity
-debug Enable debug
");

//set flags for the converter
$flags = Commandline::parse_switches($switches);

//We get nothing that resembles filename. Make it die and throw out a help screen
if (!isset($flags['filename'])) {
	die($switches['-h']);
}

$converter_instance->set_flags($flags);

//process file
try {
	$converter_instance->process_file($flags['filename']);
} catch(Exception $e) {
	print_r($e->getMessage());
	echo "\n";
}
