<?php
	//get the initial list of airports from the module
	$orig_airports = $this->get_data('airport_orig');
	$dest_airports = $this->get_data('airport_dest'); //destination uses filters that are origin-dependant

	//show search_results
	$search_results = $this->get_data('search_results');

	$sel_origin = $this->get_data('sel_origin');
	$sel_dest   = $this->get_data('sel_dest');

?>
<!DOCTYPE html><html lang="en">
<head>
	<title>M-Exam Booking application</title>	
	<link href="//netdna.bootstrapcdn.com/bootswatch/3.1.1/spacelab/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script src="assets/js/script.jquery.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
</head>

<!DOCTYPE html><html lang="en">
<head>
<body>
	<div class="navbar navbar-default ">
	  <div class="container">
		<div class="navbar-header">
		</div>
		<div class="navbar-collapse collapse navbar-responsive-collapse">
		</div>
	  </div>
	</div>

	<div class="bs-docs-section">
		<div class="container">
			<h1>Pick a flight</h1>
		</div>
		<div class="container" style="margin-top:25px;">
			<div >

			<form method="post" action="index.php" class="well form-search" role="form">
				<div class="form-group form-inline">
					<strong>Origin: </strong><select name="airport_origin" id="airport-origin" class="form-control" onchange="get_destination_airports();">
						<option value="-1" disabled="disabled">Select point of origin</option>
						<option value="0">Any</option>
						<?php
							foreach($orig_airports as $airport) {
								$selected = ($sel_origin == $airport['airport_id']) ? 'selected="selected"' : '';
								echo "<option value='$airport[airport_id]' $selected>$airport[symbol]</option>\n";
							}
						?>
					</select>
					<strong>Destination: </strong><select name="airport_dest" id="airport-dest" class="form-control">
						<option value="-1"  disabled="disabled">Select destination</option>
						<option value="0">Any</option>
						<?php
							foreach($dest_airports as $airport) {
								$selected = ($sel_dest == $airport['airport_id']) ? 'selected="selected"' : '';
								echo "<option value='$airport[airport_id]' $selected>$airport[symbol]</option>\n";
							}
						?>
					</select>
					<input type="submit" class="btn btn-success" value="Find" class="form-control" />
				</div>
			</form>
		</div>

		<div class="panel panel-default" id="book-flight-panel" style="display:none;">
			<div class="panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-book-flight-panel-close">×</button>
				<h3 class="panel-title">Book flight - <strong><span id="flight-number"></span></strong></h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" id="form-new-client" onsubmit="return false;">
				  <fieldset>
				    <div class="form-group">
				      <div class="col-lg-2">
				        <strong>From:</strong> <span id="flight-orig"></span> 
				      </div>
				      <div class="col-lg-2">
				        <strong>To:</strong> <span id="flight-dest"></span> 
				      </div>
				      <div class="col-lg-2">
				        <strong>Carrier:</strong> <span id="flight-carrier"></span>
				      </div>
				      <div class="col-lg-2">
				        <strong>Price:</strong> <span id="flight-price"></span>
				      </div>
				      <div class="col-lg-2">
				        <strong>Departure:</strong> <span id="flight-departure"></span>
				      </div>
				      <div class="col-lg-2">
				        <strong>Time:</strong> <span id="flight-time"></span>
				      </div>
				    </div>
				    <hr />
				    <div class="form-group">
				      <label for="input-address" class="col-lg-1 control-label">Seats:</label>
				      <div class="col-lg-1">
				        <input type="text" class="form-control" id="input-seats" placeholder="">
				      </div>
				      <div class="col-lg-1">
				        (max: <span id="flight-seats" placeholder="0"></span>)
				      </div>
				    </div>
					<div class="form-group">
						<div class="alert alert-dismissable" id="book-flight-alert" style="display:none;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<span id="book-flight-alert-message">ponies?</span>
						</div>
					</div>
				    <div class="form-group">
				      <div class="col-lg-11">
				        <input type="reset" class="btn btn-default" value="Reset" />
				        <a href="javascript:book_flight();" class="btn btn-success">Book flight</a>
				      </div>
				    </div>
				  </fieldset>
				</form>
			</div>
		</div>

		<table id="table1" class="table table-striped table-bordered table-hover">
	    <thead>
		<tr>
			<th>Flight number</th>
			<th>Origin</th>
			<th>Destination</th>
			<th>Carrier</th>
			<th>Departure</th>
			<th>Time</th>
			<th>Flight length</th>
			<th>Price</th>
			<th>Free seats</th>
			<th>Book flight</th>
		</tr>
	    </thead>
	    <tbody>
	    <?php
	    	//iterate through search results
	    	foreach($search_results as $row) {
	    		//[carrier_id] => 2
	    		echo "<tr id='row-$row[flight_number]'>
	    		<td>$row[flight_number]</td>
	    		<td>$row[origin_symbol]</td>
	    		<td>$row[destination_symbol]</td>
	    		<td>$row[carrier_name]</td>
	    		<td>$row[flight_day]</td>
	    		<td>$row[flight_time]</td>
	    		<td>$row[duration]</td>
	    		<td>$row[price]</td>
	    		<td>$row[seats]</td>
	    		<td><a href=\"javascript:void(open_book_flight_dialogue('$row[flight_number]'));\">Book flight</a></td></tr>";
	    	}
	    ?>
		</tbody>
		</table>
		</div> 
	</div>
</body>
</html>
