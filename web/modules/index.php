<?php

//require database module
require_once '../core/dbcore.php';

class Module_Index {
	/**
	* Constructor initializes database 
	*/
	public function __construct() {
		$this->db = DBCore::get_instance();
	}

	/**
	* Get airports
	* 
	* @return array list of airports
	*/
	public function get_airports($origin_id = null) {
		//filter destination entries if the origin is selected
		if (!$origin_id) {
			return $this->db->fetch("SELECT * FROM airports ORDER BY symbol DESC");
		} else {
			$sql ="SELECT a.* FROM airports a
				LEFT JOIN flights f ON (f.destination_airport_id = a.airport_id)
				WHERE f.origin_airport_id = ?
				GROUP BY f.destination_airport_id
			";
			 return $this->db->fetch($sql, array($origin_id));
		}
	}

	/**
	* Get search results
	* 
	* @return array list of results
	*/
	public function results($origin, $destination) {
		//We  don't need to display anything if there's no origin or destination
		if (!$origin && !$destination) {
			return array();
		}

		$add_where = '';
		$where_binds = array();
		if($origin) {
			$add_where .= ' AND f.origin_airport_id = :origin_airport_id ';
			$where_binds['origin_airport_id'] = $origin;
		}

		if($destination) {
			$add_where .= ' AND f.destination_airport_id = :destination_airport_id ';
			$where_binds['destination_airport_id'] = $destination;
		}

		$sql = "SELECT f.*, ao.symbol AS origin_symbol, ad.symbol AS destination_symbol, count(*) AS seats, c.short_name AS carrier_name
			FROM flights f
			LEFT JOIN airports ao ON (f.origin_airport_id = ao.airport_id)
			LEFT JOIN airports ad ON (f.destination_airport_id = ad.airport_id)
			LEFT JOIN carriers c ON (c.carrier_id = f.carrier_id)
			WHERE f.seat_used = 0
			$add_where
			GROUP BY f.flight_number
		";

		return $this->db->fetch($sql, $where_binds);
	}
}