<?php

//require database module
require_once '../core/dbcore.php';

//require database module
require_once '../classes/class.m_exception.php';


class Module_Ajax {
	/**
	* Constructor initializes database 
	*/
	public function __construct() {
		$this->db = DBCore::get_instance();
	}

	/**
	* Get destination airports according to origin ID
	* 
	* @return array list of airports
	*/
	public function get_origin_airports($destination_id) {
		if (!$destination_id) {
			return $this->db->fetch("SELECT * FROM airports ORDER BY symbol DESC");
		} else {
			$sql ="SELECT a.* FROM airports a
				LEFT JOIN flights f ON (f.origin_airport_id = a.airport_id)
				WHERE f.destination_airport_id = ?
				GROUP BY f.origin_airport_id
			";
			 return $this->db->fetch($sql, array($destination_id));
		}
	}

	/**
	* Get destination airports according to origin ID
	* 
	* @return array list of airports
	*/
	public function get_destination_airports($origin_id) {
		if (!$origin_id) {
			return $this->db->fetch("SELECT * FROM airports ORDER BY symbol DESC");
		} else {
			$sql ="SELECT a.* FROM airports a
				LEFT JOIN flights f ON (f.destination_airport_id = a.airport_id)
				WHERE f.origin_airport_id = ?
				GROUP BY f.destination_airport_id
			";
			 return $this->db->fetch($sql, array($origin_id));
		}
	}

	/**
	* Get flight information
	* 
	* @return array flight info
	*/
	public function get_flight_info($flight_number) {
		$sql = "SELECT f.*, ao.symbol AS origin_symbol, ad.symbol AS destination_symbol, count(*) AS seats, c.short_name AS carrier_name
			FROM flights f
			LEFT JOIN airports ao ON (f.origin_airport_id = ao.airport_id)
			LEFT JOIN airports ad ON (f.destination_airport_id = ad.airport_id)
			LEFT JOIN carriers c ON (c.carrier_id = f.carrier_id)
			WHERE f.flight_number = ?
			AND seat_used = 0
			GROUP BY f.flight_number
		";

		return $this->db->fetch($sql, array($flight_number), true);
	}

	/**
	* book seats
	* 
	* @return integer number of seats
	*/
	public function book_seats($flight_number, $seats) {
		$seats = (int) $seats; //typecast the data to strip all the unwanted garbage
		if($seats <= 0) { //There can't book 0 or less seats. Kicking out by default
			throw new M_Exception('Invalid input data!', M_Exception::ERROR_INVALID_INPUT_DATA);
		}

		//check the number of seats before doing anything else
		$query = $this->db->fetch("SELECT count(*) AS cnt FROM flights f WHERE f.flight_number = ? AND seat_used = 0", array($flight_number), true);
		if ($query['cnt'] < $seats) {
			throw new M_Exception('That number of seats for this flight is not available!', M_Exception::ERROR_INVALID_INPUT_DATA);
		}

		return $this->db->update("UPDATE flights 
			SET seat_used = 1 
			WHERE flight_number = ? AND seat_used = 0 
			ORDER BY id
			LIMIT $seats
			", array($flight_number), true);
	}
}
