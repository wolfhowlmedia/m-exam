<?php
//require configure 
require_once '../config.php';

//rrequire common functions, mostly the router.
require_once '../core/common.php';

//require requests (mostly POST)
require_once '../core/request.php';

//require Index module
require_once 'modules/ajax.php';

//resolve the actions
list($http_method, $action) = Common::path_router();

$output = array();
//we only allow these calls via post
if($http_method == "post") {
	$ajax = new Module_Ajax;
	switch($action) {
		case 'get_destination_airports':
			try {
				$output = array(
					'error' => false,
					'data'  => $ajax->get_destination_airports(POST::ret('origin_airport'))
				);
			} catch(Exception $e) {
				$output = array(
					'error' => true,
					'error_msg' => $e->getMessage()
				);
			}
		break;

		case 'get_origin_airports':
			try {
				$output = array(
					'error' => false,
					'data'  => $ajax->get_origin_airports(POST::ret('destination_airport'))
				);
			} catch(Exception $e) {
				$output = array(
					'error' => true,
					'error_msg' => $e->getMessage()
				);
			}
		break;

		case 'get_flight_info':
			try {
				$output = array(
					'error' => false,
					'data'  => $ajax->get_flight_info(POST::ret('flight_number'))
				);
			} catch(Exception $e) {
				$output = array(
					'error' => true,
					'error_msg' => $e->getMessage()
				);
			}
		break;

		case 'book_seats':
			try {
				$output = array(
					'error' => false,
					'data' => $ajax->book_seats(POST::ret('flight_number'), POST::ret('seats'))
				);
			} catch(Exception $e) {
				$output = array(
					'error' => true,
					'error_msg' => $e->getMessage()
				);
			}
		break;
	}
}
echo json_encode($output);
