<?php
//require configure 
require_once '../config.php';

//require templating system
require_once '../core/template.php';

//require templating system
require_once '../core/request.php';

//require Index module
require_once 'modules/index.php';

$index = new Module_Index;

$tpl = new Template('index.tpl.php');

//send data from module to template
$tpl->set_data('sel_origin', POST::ret('airport_origin'));
$tpl->set_data('sel_dest', POST::ret('airport_dest'));

$tpl->set_data('search_results', $index->results(POST::ret('airport_origin'), POST::ret('airport_dest')));
$tpl->set_data('airport_orig', $index->get_airports());
$tpl->set_data('airport_dest', $index->get_airports(POST::ret('airport_origin')));
echo $tpl->render();