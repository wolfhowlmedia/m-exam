/**
* Get destination airports
* 
*
**/
function get_destination_airports() {
	var destination = $("#airport-dest");
	destination.html('<option value="-1"  disabled="disabled">Select destination</option><option value="0" enabled="">Any</option>');
	var origin_id = $("#airport-origin").val();

	//AJAX POST call
	$.ajax({
		url: "ajax.php/post/get_destination_airports",
		type:'post',
		data: {'origin_airport' : origin_id}
	}).done( function(data) { //fill the dropdown box with fitting destination fligs
		json_out = $.parseJSON(data);
		if(!json_out.error) {
			for (var x = 0; x < json_out.data.length; x++) {
				destination.append('<option value="' + json_out.data[x].airport_id + '">' + json_out.data[x].symbol + '</option>');
			}
		} else {
			alert(json_out.error_msg);
		}
	});
}

function open_book_flight_dialogue(flight_number) {
	//Traverse through all lines and unhighlight the selected line
	$('tr').each(function(index) {
		$(this).removeClass('info');
	});

	$("#row-" + flight_number).addClass('info');
	//AJAX POST call
	$.ajax({
		url: "ajax.php/post/get_flight_info",
		type:'post',
		data: {'flight_number' : flight_number}
	}).done( function(data) { //
		json_out = $.parseJSON(data);
		if(!json_out.error) {
			$('#flight-number').text(json_out.data.flight_number);
			$('#flight-orig').text(json_out.data.origin_symbol);
			$('#flight-dest').text(json_out.data.destination_symbol);
			$('#flight-carrier').text(json_out.data.carrier_name);
			$('#flight-price').text(json_out.data.price);
			$('#flight-departure').text(json_out.data.flight_day);
			$('#flight-time').text(json_out.data.flight_time);
			$('#flight-seats').text(json_out.data.seats);
		} else {
			alert(json_out.error_msg);
		}
	});

	$('#book-flight-panel').show(100);
}

function book_flight() {
	flight_number = $('#flight-number').text();
	seats = $('#input-seats').val();

	$.ajax({
		url: "ajax.php/post/book_seats",
		type:'post',
		data: {'flight_number' : flight_number, 'seats' : seats}
	}).done( function(data) {
		json_out = $.parseJSON(data);
		if(!json_out.error) {
			//display success message bar
			$("#book-flight-alert").removeClass('alert-danger');
			$("#book-flight-alert").addClass('alert-success');
			$("#book-flight-alert-message").text("Succesfully booked " + seats + " seats");
			$("#book-flight-alert").show();
		} else {
			//display failure message bar
			$("#book-flight-alert").removeClass('alert-success');
			$("#book-flight-alert").addClass('alert-danger');
			$("#book-flight-alert-message").text(json_out.error_msg);
			$("#book-flight-alert").show();
		}
	});
}

jQuery( document ).ready(function( $ ) {
	//hide the detail panel when clicking the 'x'
	$( "#btn-book-flight-panel-close" ).click(function() {
		//Traverse through all lines and unhighlight the selected line
		$('tr').each(function(index) {
			$(this).removeClass('info');
		});

		//hide the panel
		$('#book-flight-panel').hide(100);
	});

});