<?php

class M_Exception extends Exception {
	//default error numbers that can be awoken by self::nameoftheconstant
	const ERROR_FILE_CHECK_FAILED   = 100;

	const ERROR_FILE_OPENING_FAILED = 101;

	const ERROR_INVALID_INPUT_DATA  = 102;


    /**
     * Constructor
     *
     * @param strong Exception message
     * @param int Exception code
     */
	public function __construct($message, $code) {
		//call parent constructor so the message can be ralayed to PHP's Exception handler
		parent::__construct($message, $code);
	}
}