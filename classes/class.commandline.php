<?php
class Commandline {
	/**
	* A simple comman d line parser
	*
	* @param array switches 
	* 
	* @return array of flags or void
	*/
	public static function parse_switches($switches) {
		$flags = array();
		if(!isset($GLOBALS['argv'])) {
			//we check if this is executed from elsewhere, but instead of  
			//killing the script right away, we return an empty array
			return array();
		}

		//copy CLI arguments to the local variable
		$cli_arguments = $GLOBALS['argv'];
		unset($cli_arguments[0]); //Removing the first flag to not be confused with the filename directive

		foreach($cli_arguments as $arg) {
			if (isset($switches[$arg])) { //We have an argument present
				//check if the argumanets are array so they can be effectively set as flags
				if(is_array($switches[$arg])) {
					$flags += $switches[$arg];
				} else if (is_string($switches[$arg])) { //the argument is string (help flag usually). Display it and exit.
					echo $switches[$arg];
					die;
				}
			} else { //we treat the remaining argument as filename
				$flags['filename'] = $arg;
			}
		}


		return $flags;
	}
}
