<?php
//require DB configuration
require_once '../config.php';

//import DB functionality
require_once '../core/dbcore.php';

//throw custom exceptions
require_once '../classes/class.m_exception.php';

//core converter class
class Converter {
	//initialize variables
	private $db         = null;
	private $check_file = true;
	private $dry_run    = false;
	private $output     = array();
	private $debug      = true;


	//initialize everything
	public function __construct() {
		$this->db = DBCore::get_instance();
	}


	/**
	* Set flags for file processing
	*
	* @param array $flags set apropriate flags
	*/
	public function set_flags($flags) {
		if (isset($flags['dry_run'])) {
			$this->dry_run = (bool)$flags['dry_run'];
		}

		if (isset($flags['check_file'])) {
			$this->check_file = (bool)$flags['check_file'];
		}

		if (isset($flags['debug'])) {
			$this->check_file = (bool)$flags['debug'];
		}
	}

	/**
	* Process file altogether
	*
	* @param string filename
	*/
	public function process_file($filename) {
		//Try opening the file
		if(!file_exists($filename)) {
			throw new M_Exception("File $filename doesn't exist!", M_Exception::ERROR_FILE_OPENING_FAILED);
		}

		$file_data = file($filename);

		//check file content. Can be skipped via appropriate flag
		if ($this->check_file && is_array($err = $this->check_file_content($file_data))) {
			throw new M_Exception('File content check failed on line: ' . $err['line_no'], M_Exception::ERROR_FILE_CHECK_FAILED);
		}

		//add the content of the file to the database
		foreach($file_data as $line) {
			$this->process_line($line);
		}

	}


	/**
	* Toss out the processing data
	*
	* @return output of the processing file
	*
	*/
	public function output() {
		return $this->output;
	}


	/**
	* Check file content
	*
	* @param array file_data
	* 
	* @return array or true
	*/
	private function check_file_content(array $file_data) {
		//check each line for consistency:
		foreach($file_data as $line_no => $line) {
			$line = trim($line);
			//ignore empty lines
			if ($line) {
				//if the line doesn't consists of strictly 9 elements, then there's something wrong with it
				$row = explode("^", $line);
				if (!$this->check_row($row)) {
					return array(
						'line_no' => $line_no + 1,
						'error'   => true
					);
				}
			}
		}

		return true;
	}


	/**
	* Process each line and insert the content into database
	*
	* @param string filename
	*/
	private function process_line($line_data) {
		$line_data = trim($line_data);
		$row = explode('^', trim($line_data));
		/**
		* Row structure:
		* 0 - Flight number
		* 1 - Origin airport
		* 2 - Destination airport
		* 3 - Carrier name
		* 4 - Price
		* 5 - Day
		* 6 - Time
		* 7 - Duration
		* 8 - Seats
		**/

		//We have to skip erroneous rows
		if(!$this->check_row($row)) {
			$this->add_message_to_pool('Found wrong type of row. Skipping.', 'error');
			return;
		} else if (!$row[8]) { //Also skip if there's an entry with 0 seats available
			$this->add_message_to_pool('Found a flight without seat number. Skipping.', 'error');
			return;
		}

		//check if flight information had already been inserted by flight number
		$out = $this->db->fetch("SELECT COUNT(*) AS cnt FROM flights WHERE flight_number LIKE ?", array($row[0]), true);
		if (!$out['cnt']) {
			$this->add_message_to_pool("Flight number $row[0] not in database yet");
		} else {
			$this->add_message_to_pool("Flight $row[0] already in database. Skipping!", 'error');
			return;
		}

		//before we insert everything else, we need to get the IDs of accompanying airports and carriers
		$origin_airport_id      = $this->get_airport_id($row[1]); //get the origin airport ID
		$destination_airport_id = $this->get_airport_id($row[2]); //get the destination airport ID
		$carrier_id             = $this->get_carrier_id($row[3]); //get the carrier ID

		//SQL binding array
		$flight_info = array(
			$row[0],
			$origin_airport_id,
			$destination_airport_id, 
			$carrier_id,
			$row[4],
			$row[5],
			$row[6],
			$row[7],
		);

		//insert each seat individually
		if(!$this->dry_run) {
			for($x = 0; $x < $row[8]; $x++) {
				$sql = 'INSERT INTO flights (flight_number, origin_airport_id, destination_airport_id, carrier_id, price, flight_day, flight_time, duration)
					VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
				$this->db->insert($sql, $flight_info);
			}
		} else {
			$this->add_message_to_pool("[DRY RUN] Skipping inserting data.", 'info');			
		}

		$this->add_message_to_pool("Added flight $row[0] from $row[1] to $row[2] with $row[8] seats.");
	}


	/**
	* Get airport ID
	*
	* @param string symbol
	*
	* @return integer airport ID
	*/
	private function get_airport_id($symbol) {
		$out = $this->db->fetch("SELECT airport_id FROM airports WHERE symbol LIKE ?", array($symbol), true);
		if ($out) { //We already have the airport in the database
			$this->add_message_to_pool('Airport with symbol ' . $symbol . ' already in the database.', 'info');
			return $out['airport_id'];
		}

		if(!$this->dry_run) {
			return $this->db->insert('INSERT INTO airports (symbol) VALUES (?)', array($symbol));
		} else {
			$this->add_message_to_pool('[DRY RUN] Skipping inserting airport ' . $symbol . ' into database.', 'info');
			return -1;
		}
	}

	/**
	* Get carrier ID
	*
	* @param string carrier
	*
	* @return integer carrier ID
	*/
	private function get_carrier_id($carrier) {
		$out = $this->db->fetch("SELECT carrier_id FROM carriers WHERE short_name LIKE ?", array($carrier), true);
		if ($out) { //We already have the airport in the database
			$this->add_message_to_pool('Airport with symbol ' . $carrier . ' already in the database.', 'info');
			return $out['carrier_id'];
		}

		if(!$this->dry_run) {
			return $this->db->insert('INSERT INTO carriers (short_name) VALUES (?)', array($carrier));
		} else {
			$this->add_message_to_pool('[DRY RUN] Skipping inserting carrier ' . $carrier . ' into database.', 'info');
			return -1;
		}
	}

	/**
	* Add message to the pool of messages
	*
	* @param string filename
	*
	* @return void
	*/
	private function add_message_to_pool($message, $type = 'ok') {
		$this->output[] = array(
			'type'   => $type,
			'message' => $message
		);

		if ($this->debug) {
			switch($type) {
				case 'error':
					$color = "[31m";
				break;
				case 'ok':
					$color = "[32m";
				break;
				case 'info':
					$color = "[33m";
				break;
			}

			echo chr(27) . "$color"."$message" . chr(27) . "[0m\n";
		}
	}

	/**
	* Check row integrity
	*
	* @param array row
	*
	* @return bool
	*/
	private function check_row($row) {
		$out = true;
		if(sizeof($row) != 9) {
			$out = false;
		} else {
			//Additional check if origin and destination symbol consists of 3 letters
			if (strlen($row[1]) !== 3 || strlen($row[2]) !== 3) {
				$out = false;
			}
		}

		return $out;
	}
}
