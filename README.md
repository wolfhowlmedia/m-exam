
# README

**This is used solely as a test application.**

Structure:
 1. core - Core libraries are modified / fixed libraries from Core9
   * common   - common tools
   * dbcore   - lightweight database layer based on PDO
   * request  - class dealing with POST / GET / REQUEST / COOKIE requests
   * template - basic templating tool
 2. web
   * assets/  - web assets. In this case there are only Javascript files
   * modules/
     * ajax   - module for XHR requests
     * index  - module for index page. Handles searches 
   * templates/ template for index page, relying on bootstrap and jQuery (external libs)
   * ajax   - module loaders for respective pages
   * index  - module loaders for respective pages
 3. classes
   * commandline - a simple command line interface for handling switches
   * converter   - backend for the converter tool
   * m_exception - custom exception that supports some inline error codes
 4. tools - front-end for converter tool; a database importer
 5. misc
   * database_structure - MySQL DB structure
   * sample_data - sample data grabbed from the instruction page

**Web interface**

Web interface is made with Boostrap for ease of structuring and templating. With that, jQuery is also used as a javascripting library, especially to handle XHR requests.

**Converter**

Converter is a data importing tool. It checks and parses the provided file line-by-line and inserts it into the database.

Command-line switches:
```
-d     Enable dry run (no data is inserted into database)
-c     Skip checking file integrity
-debug Enable debug
```
